<?php

/**
 * Created by PhpStorm.
 * User: Lina
 * Date: 08.05.2016
 * Time: 18:52
 */
class UserController extends BaseController
{
    public function setLogin()
    {
        if (Auth::check()) {
            return Redirect::to('Profile');
        }
        $user = new User();
        if (Input::has('email')) {
            $email = Input::get('email');
            $password = Input::get('password');

            $validator = Validator::make(
                array(
                    'email' => $email,
                    'password' => $password
                ),
                array(
                    'email' => 'required|email|min:3|max:40',
                    'password' => 'required|min:6|max:12'
                ),
                array(
                    'required' => 'Attention! The :attribute field are necessary to fill ',
                    'email' => 'Attention! Incorrect e-mail field',
                    'min' => 'Attention! The :attribute field must be over then :min chars',
                    'max' => 'Attention! The :attribute field must be less then :max chars',
                )
            );


            if ($validator->fails()) {
                $emailErrors = '';
                $passwordErrors = '';

                $messages = $validator->messages();
                if ($messages->has('email')) {
                    foreach ($messages->get('email') as $message) {
                        $emailErrors .= $message . "\n" . nl2br("\n");
                    }
                }
                if ($messages->has('password')) {
                    foreach ($messages->get('password') as $message) {
                        $passwordErrors .= $message . "\n" . nl2br("\n");
                    }
                }
            } else {
                var_dump(Auth::attempt(array('email' => $email, 'password' => $password)));
                if (Auth::attempt(array('email' => $email, 'password' => $password))) {

                    return Redirect::intended('Profile');
                }
                //return Redirect::to ('Registration2');
            }
        };
        return View::make('LoginPage', array('title' => 'Login page',
            'usernameErrors' => isset($usernameErrors) ? $usernameErrors : null,
            'passwordErrors' => isset($passwordErrors) ? $passwordErrors : null,
        ));
    }

    public function setEmailUsernamePassword()
    {
        if (Auth::check()) {
            return Redirect::to('Profile');
        }
        if (Input::has('check')) {
            $username = Input::get('username');
            $email = Input::get('email');
            $password = Input::get('password');


            Session::put('username', $username);
            Session::put('email', $email);
            Session::put('password', $password);
            Session::put('password1', $password);

            $validator = Validator::make(
                array(
                    'username' => $username,
                    'email' => $email,
                    'password' => $password
                ),
                array(
                    'username' => 'required| unique:users,username',
                    'email' => 'required| unique:users,email|email|min:3|max:40',
                    'password' => 'required|min:6|max:12'
                ),
                array(
                    'required' => 'Attention! The :attribute field are necessary to fill ',
                    'email' => 'Attention! Incorrect e-mail field',
                    'unique' => 'Attention! Not unique :attribute field',
                    'min' => 'Attention! The :attribute field must be over then :min chars',
                    'max' => 'Attention! The :attribute field must be less then :max chars',
                )
            );
            if ($validator->fails()) {
                $usernameErrors = '';
                $emailErrors = '';
                $passwordErrors = '';

                $messages = $validator->messages();
                if ($messages->has('username')) {
                    foreach ($messages->get('username') as $message) {
                        $usernameErrors .= $message . "\n" . nl2br("\n");
                    }
                }
                if ($messages->has('email')) {
                    foreach ($messages->get('email') as $message) {
                        $emailErrors .= $message . "\n" . nl2br("\n");
                    }
                }
                if ($messages->has('password')) {
                    foreach ($messages->get('password') as $message) {
                        $passwordErrors .= $message . "\n" . nl2br("\n");
                    }
                }
            } else {
                return Redirect::to('Registration2');
            }
        }
        return View::make('setFirstInformation',
            array('title' => 'Registration page',
                'emailErrors' => isset($emailErrors) ? $emailErrors : null,
                'usernameErrors' => isset($usernameErrors) ? $usernameErrors : null,
                'passwordErrors' => isset($passwordErrors) ? $passwordErrors : null,
            )
        );

    }

    public function setNameSecondNameGenderAbout()
    {
        if (Auth::check()) {
            return Redirect::to('Profile');
        }
        if (Input::has('check1')) {
            $firstName = Input::get('firstName');
            $lastName = Input::get('lastName');
            $gender = Input::get('gender');
            $about = Input::get('about');

            Session::put('firstName', $firstName);
            Session::put('lastName', $lastName);
            Session::put('gender', $gender);
            Session::put('about', $about);

            $validator = Validator::make(
                array(
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                ),
                array(
                    'firstName' => 'required|regex:"[a-zA-Z\s\"]+" ',
                    'lastName' => 'required|regex:"[a-zA-Z\s\"]+" ',
                ),
                array(
                    'required' => 'Attention! The :attribute field are necessary to fill ',
                    'regex' => 'Attention! Incorrect :attribute field',
                )
            );

            if ($validator->fails()) {
                $firstNameErrors = '';
                $lastNameErrors = '';

                $messages = $validator->messages();
                if ($messages->has('firstName')) {
                    foreach ($messages->get('firstName') as $message) {
                        $firstNameErrors .= $message . "\n" . nl2br("\n");
                    }
                }
                if ($messages->has('lastName')) {
                    foreach ($messages->get('lastName') as $message) {
                        $lastNameErrors .= $message . "\n" . nl2br("\n");
                    }
                }
            } else {

                return Redirect::to('Confirmation');
            }
        }
        return View::make('setSecondInformation', array('title' => 'User Registration',
            'firstNameErrors' => isset($firstNameErrors) ? $firstNameErrors : null,
            'lastNameErrors' => isset($lastNameErrors) ? $lastNameErrors : null,
        ));
    }

    public function confirmationUserData()
    {
        $user = new User();

        if (Input::has('checkConformation')) {

            $user->fill(Input::all());
            //$user -> save();
            $user->signup();
            //Auth::attempt();
            Auth::login($user);
        }
        if (Auth::check()) {
            return Redirect::to('Profile');
        }
        return View::make('confirmation', array('title' => 'User Registration'));
    }

    public function ownProfile()
    {
        if (Input::has('checkLogout')) {
            Auth::logout();
            Session::flush();
            return Redirect::to('SetLogin');
        } else {
            return View::make('profile', array('title' => 'User\'s profile'));
        }
    }

    public function editProfile()
    {
        $id = Auth::user()->id;
        //var_dump($id);

        if (Input::has('EditCheck')) {

            $username = Input::get('username');
            $email = Input::get('email');
            $firstName = Input::get('firstName');
            $lastName = Input::get('lastName');
            $gender = Input::get('gender');
            $about = Input::get('about');

            //$password = Input::get('password');
            // $passwordNew = Input::get('passwordNew');


            $validator = Validator::make(
                array(
                    'username' => $username,
                    'email' => $email,
                    //'password' => $password,
                    //'passwordNew' => $passwordNew,
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                ),
                array(
                    'username' => "required| unique:users,username,$id",
                    'email' => "required| unique:users,email,$id|email|min:3|max:40",
                    //'password' => 'min:6|max:12',
                    // 'passwordNew' => 'min:6|max:12',
                    'firstName' => 'required|regex:"[a-zA-Z\s\"]+" ',
                    'lastName' => 'required|regex:"[a-zA-Z\s\"]+" ',
                ),
                array(
                    'required' => 'Attention! The :attribute field are necessary to fill ',
                    'email' => 'Attention! Incorrect e-mail field',
                    'unique' => 'Attention! Not unique :attribute field',
                    'min' => 'Attention! The :attribute field must be over then :min chars',
                    'max' => 'Attention! The :attribute field must be less then :max chars',
                    'regex' => 'Attention! Incorrect :attribute field',
                )
            );
            if ($validator->fails()) {
                $usernameErrors = '';
                $emailErrors = '';
                //$passwordErrors='';
                // $newpasswordErrors='';
                $firstNameErrors = '';
                $lastNameErrors = '';

                $messages = $validator->messages();
                if ($messages->has('username')) {
                    foreach ($messages->get('username') as $message) {
                        $usernameErrors .= $message . "\n" . nl2br("\n");
                    }
                }
                if ($messages->has('email')) {
                    foreach ($messages->get('email') as $message) {
                        $emailErrors .= $message . "\n" . nl2br("\n");
                    }
                }
                /*if ($messages->has('password')) {
                    foreach ($messages->get('password') as $message) {
                        $passwordErrors.= $message ."\n". nl2br("\n");
                    }
                }
                if ($messages->has('passwordNew')) {
                    foreach ($messages->get('passwordNew') as $message) {
                        $newpasswordErrors.= $message ."\n". nl2br("\n");
                    }
                }*/
                if ($messages->has('firstName')) {
                    foreach ($messages->get('firstName') as $message) {
                        $firstNameErrors .= $message . "\n" . nl2br("\n");
                    }
                }
                if ($messages->has('lastName')) {
                    foreach ($messages->get('lastName') as $message) {
                        $lastNameErrors .= $message . "\n" . nl2br("\n");
                    }
                }
            } else {

                Auth::user()->username = $username;
                Auth::user()->email = $email;
                Auth::user()->firstName = $firstName;
                Auth::user()->lastName = $lastName;
                Auth::user()->gender = $gender;
                Auth::user()->about = $about;
                Auth::user()->save();

            }

        } else {
            $username = Auth::user()->username;
            $email = Auth::user()->email;
            $firstName = Auth::user()->firstName;
            $lastName = Auth::user()->lastName;
            $gender = Auth::user()->gender;
            $about = Auth::user()->about;
            $password = Auth::user()->password;

            Session::put('username', $username);
            Session::put('email', $email);
            Session::put('firstName', $firstName);
            Session::put('lastName', $lastName);
            Session::put('gender', $gender);
            Session::put('about', $about);
        }
        return View::make('edit', array('title' => 'Edit user\'s profile',
            'emailErrors' => isset($emailErrors) ? $emailErrors : null,
            'usernameErrors' => isset($usernameErrors) ? $usernameErrors : null,
            'passwordErrors' => isset($passwordErrors) ? $passwordErrors : null,
            'firstNameErrors' => isset($firstNameErrors) ? $firstNameErrors : null,
            'lastNameErrors' => isset($lastNameErrors) ? $lastNameErrors : null,
        ));
    }
}