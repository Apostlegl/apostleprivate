<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/*
Route::get('/', function()
{
	return View::make('hello');
});*/
Route::get('/','UserController@setLogin');
// ��������� �������
Route::get('/env', function() {
	return App::environment();
});
Route::any('SetLogin', 'UserController@setLogin');
Route::any('Registration', 'UserController@setEmailUsernamePassword');
Route::any('Registration2', 'UserController@setNameSecondNameGenderAbout');
Route::any('Confirmation', 'UserController@confirmationUserData');
Route::any('Profile',array('before'=>'auth', 'uses'=>'UserController@ownProfile'));
Route::any('EditProfile',array('before'=>'auth', 'uses'=>'UserController@editProfile'));

