<?php include __DIR__.'/header.php'; ?>
    <center>
        <h1> Confirmation registration data</h1>
        <?php echo Form::open(array('method' => 'post')); ?>
        <p>
            <b style="color:grey">Username </b>
            <?php echo Form::text('username', Session::get('username'), array( 'readonly')); ?>
        </p>
        <p>
            <b style="color:grey">E-mail...... </b>
            <?php echo Form::email('email', Session::get('email'), array( 'readonly')); ?>
        </p>
        <p>
            <b style="color:grey">Password: </b>
            <?php echo Form::text('password1',Session::get('password1'), array( 'readonly')); ?>
        </p>
        <p>
            <b style="color:grey">FirstName </b>
            <?php echo Form::text('firstName', Session::get('firstName'), array( 'readonly')); ?>
        </p>
        <p>
            <b style="color:grey">LastName </b>
            <?php echo Form::text('lastName', Session::get('lastName'), array( 'readonly')); ?>
        </p>
        <p>
            <b style="color:grey">Your gender - </b>
            <?php echo Form::select('gender', array('Male' => 'Male', 'Female' => 'Female'), Session::get('gender')); ?>
        </p>
        <p>
            <b style="color:grey">About you</b>
        </p>
        <p>
            <?php echo Form::textarea('about', Session::get('about'), array( 'readonly')) ?>
        </p>
        <p>
            <?php echo Form::hidden('checkConformation',1) ?>
        </p>
        <a href='/Registration2'><button type="button">Back </button></a>
        <?php echo Form::button("Finish", array ('type' => 'submit')); ?>
        <?php echo Form::close(); ?>
    </center>
<?php include __DIR__.'/footer.php'; ?>