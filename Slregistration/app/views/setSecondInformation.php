<?php include __DIR__.'/header.php'; ?>
<center>
    <h1> Registration form 2/3 </h1>
    <h5> Some fields are necessary to fill </h5>
    <?php echo Form::open(array('method' => 'post')); ?>
    <p>
        <?php echo isset ($firstNameErrors) ? "<p style=\"color:red\">" . $firstNameErrors . "</p>" : null; ?>
    </p>
    <p>
        <?php echo Form::text('firstName', Session::has('firstName') ? Session::get('firstName') : null, array('placeholder' =>  Session::has('firstName') ? Session::get('firstName') : 'Enter your name*', 'required')); ?>
    </p>
    <p>
        <?php echo isset ($lastNameErrors) ? "<p style=\"color:red\">" . $lastNameErrors . "</p>" : null; ?>
    </p>
    <p>
        <?php echo Form::text('lastName', Session::has('lastName') ? Session::get('lastName') : null, array('placeholder' => Session::has('lastName') ? Session::get('lastName') : 'Enter last name*', 'required')); ?>
    </p>
    <p>
        What's your gender -
        <?php echo Form::select('gender', array('Male' => 'Male', 'Female' => 'Female'), Session::has('gender') ? Session::get('gender') : 'Male'); ?>
    </p>
    <p>
        <?php echo Form::textarea('about', Session::has('about') ? Session::get('about') : null, array('placeholder' => 'Some about you')); ?>
    </p>
    <p>
        <?php echo Form::hidden('check1',1) ?>
    </p>
    <a href='/Registration'><button type="button">Back </button></a>
    <?php echo Form::button("Next", array ('type' => 'submit')); ?>
    <?php echo Form::close(); ?>
</center>
<?php include __DIR__.'/footer.php'; ?>
