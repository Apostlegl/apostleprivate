<?php include __DIR__.'/header.php'; ?>
<center>
    <h1> Login Now </h1>
    <?php echo Form::open(array('method' => 'post')); ?>
    <p>
        <!--<b style="color:grey">E-mail</b>-->
        <?php echo isset ($emailErrors) ? "<p style=\"color:red\">" . $emailErrors . "</p>" : null; ?>
    </p>
    <p>
        <?php echo Form::email('email', null, array('placeholder' => 'Enter E-mail*', 'required')); ?>
    </p>
    <p>
        <!--<b style="color:grey">Password</b>-->
        <?php echo isset ($passwordErrors) ? "<p style=\"color:red\">" . $passwordErrors . "</p>" : null; ?>
    </p>
    <p>
        <?php echo Form::password('password', array('placeholder' => 'Enter password*', 'required')); ?>
    </p>

    <?php echo Form::submit("Login", array ('type' => 'submit')); ?>
    <p>
        <span><a href="/Registration">Register</a></span>
    </p>
    <?php echo Form::close(); ?>
</center>
<?php include __DIR__.'/footer.php'; ?>

