<?php include __DIR__.'/header.php'; ?>
    <center>
        <h1> Registration form 1/3</h1>
        <h5>
            All fields are necessary to fill
        </h5>
        <?php echo Form::open(array('method' => 'post')); ?>
        <p>
            <!--<b style="color:grey">Username</b> -->
            <?php echo isset ($usernameErrors) ? "<p style=\"color:red\">" . $usernameErrors . "</p>" : null; ?>
        </p>
        <p>
            <?php echo Form::text('username', Session::has('username') ? Session::get('username') : null, array('placeholder' => Session::has('username') ? Session::get('username') : 'Enter username*', 'required')); ?>
        </p>
        <p>
            <!--<b style="color:grey">E-mail</b>-->
            <?php echo isset ($emailErrors) ? "<p style=\"color:red\">" . $emailErrors . "</p>" : null; ?>
        </p>
        <p>
            <?php echo Form::email('email', Session::has('email') ? Session::get('email') : null, array('placeholder' => Session::has('email') ? Session::get('email') : 'Enter E-mail*', 'required')); ?>
        </p>
        <p>
            <!--<b style="color:grey">Password</b>-->
            <?php echo isset ($passwordErrors) ? "<p style=\"color:red\">" . $passwordErrors . "</p>" : null; ?>
        </p>
        <p>
            <?php echo Form::password('password', array('placeholder' => 'Enter password*', 'required')); ?>
        </p>
        <p>
            <?php echo Form::hidden('check',1) ?>
        </p>
        <a href='/SetLogin'><button type="button">Back </button></a>
        <?php echo Form::submit("Next", array ('type' => 'submit')); ?>
        <?php echo Form::close(); ?>
    </center>
<?php include __DIR__.'/footer.php'; ?>

