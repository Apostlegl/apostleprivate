<?php include __DIR__.'/header.php'; ?>
<center>
    <h1> Edit profile</h1>
    <?php echo Form::open(array('method' => 'post')); ?>
    <p>
        <?php echo isset ($usernameErrors) ? "<p style=\"color:red\">" . $usernameErrors . "</p>" : null; ?>
    </p>
    <p>
        <b style="color:grey">Username </b>
        <?php echo Form::text('username', Session::get('username')); ?>
    </p>
    <p>
        <?php echo isset ($emailErrors) ? "<p style=\"color:red\">" . $emailErrors . "</p>" : null; ?>
    </p>
    <p>
        <b style="color:grey">E-mail...... </b>
        <?php echo Form::email('email', Session::get('email')); ?>
    </p>
    <p>
        <?php echo isset ($firstNameErrors) ? "<p style=\"color:red\">" . $firstNameErrors . "</p>" : null; ?>
    </p>
    <p>
        <b style="color:grey">FirstName </b>
        <?php echo Form::text('firstName', Session::get('firstName')); ?>
    </p>
    <p>
        <?php echo isset ($lastNameErrors) ? "<p style=\"color:red\">" . $lastNameErrors . "</p>" : null; ?>
    </p>
    <p>
        <b style="color:grey">LastName </b>
        <?php echo Form::text('lastName', Session::get('lastName')); ?>
    </p>
    <p>
        <b style="color:grey">Your gender - </b>
        <?php echo Form::select('gender', array('Male' => 'Male', 'Female' => 'Female'), Session::get('gender')); ?>
    </p>
    <p>
        <b style="color:grey">About you</b>
    </p>
    <p>
        <?php echo Form::textarea('about', Session::get('about')) ?>
    </p>

    <p>
        <?php echo Form::password('password', array('placeholder' => 'Enter old password*')); ?>
    </p>
    <p>
        <?php echo Form::password('passwordNew', array('placeholder' => 'Enter new password*')); ?>
    </p>
    <p>
        <?php echo Form::hidden('EditCheck',1) ?>
    </p>
    <a href='/Profile'><button type="button">Cancel</button></a>
    <?php echo Form::submit("Save", array ('type' => 'submit')); ?>
    <?php echo Form::close(); ?>
</center>
<?php include __DIR__.'/footer.php'; ?>

