<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users',function(Blueprint $table){
			$table->increments ('id');
			$table->string ('username', 45)->unique();
			$table->string ('email', 45)->unique();
			$table->string ('password', 60);
			$table->string ('firstName', 45);
			$table->string ('lastName', 45);
			$table->enum('gender', array('Male', 'Female'));
			$table->string ('about', 255);
			$table->rememberToken();
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
